# FROM node:14.9
# WORKDIR /usr/src/app
# COPY package*.json ./
# ADD package.json /usr/src/app/package.json
# RUN npm install
# RUN npm install react-scripts@1.1.0 -g
# COPY . .
# CMD ["npm ","start"];

# FROM node:13.6.0-alpine

# ARG IMAGE_CREATE_DATE
# ARG IMAGE_VERSION
# ARG IMAGE_SOURCE_REVISION

# # Create app directory
# RUN mkdir -p /usr/src/app
# WORKDIR /usr/src/app

# # Install app dependencies
# COPY package.json /usr/src/app/
# RUN npm install

# # Bundle app source
# COPY . /usr/src/app

# USER node
# CMD [ "npm", "start" ]

# FROM node:10
# WORKDIR /usr/src/app
# COPY package*.json ./
# RUN npm install
# COPY . .
# EXPOSE 8080
# CMD ["node", "app.js"]

# FROM nginx:1.17
# COPY ./build/ /usr/share/nginx/html

# pull official base image
FROM node:13.12.0-alpine

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./
COPY package-lock.json ./
RUN npm install --silent
RUN npm install react-scripts@3.4.1 -g --silent

# add app
COPY . ./

# start app
CMD ["npm", "start"]

# FROM node:10.4.3
# WORKDIR /usr/src/app
# COPY package*.json ./
# ADD package.json /usr/src/app/package.json
# RUN npm install
# RUN npm install react-scripts@1.1.0 -g
# COPY . .
# EXPOSE 3000
# CMD ["npm ","start"];